//
//    FILE: MS5611_performance.ino
//  AUTHOR: Rob Tillaart
// PURPOSE: demo application
//    DATE: 2020-06-21
//     URL: https://github.com/RobTillaart/MS5611


#include "MS5611.h"


MS5611 MS5611(0x77);   // 0x76 = CSB to VCC; 0x77 = CSB to GND

#define MAX_SAMPLES 50



uint32_t start, stop, count;

unsigned char maxsamples= MAX_SAMPLES;
float alt[MAX_SAMPLES];

float altAveraged;//altura media


void setup()
{
  Serial.begin(115200);
  Serial.print(__FILE__);
  Serial.print("MS5611 lib version: ");
  Serial.println(MS5611_LIB_VERSION);

  bool b = MS5611.begin();
  Serial.println(b ? "found" : "not found");

  count = 0;
  Serial.println("CNT\tDUR\tRES\tTEMP\tPRES");



  
  delay(1000);

}

void loop()
{
  
   
  start = millis();
  int result = MS5611.read();
  stop = millis();

  Serial.print(count);
  count++;
  Serial.print("\t");
  Serial.print(stop - start);
  Serial.print("\t");
  Serial.print(result);
  Serial.print("\t");
  Serial.print(MS5611.getTemperature(), 2);
  Serial.print("\t");
  Serial.print(MS5611.getPressure(), 2);
 


    double realTemperature = MS5611.getTemperature();
     double realPressure = MS5611.getPressure();

     //
 
    double seaLevelPressure = 1013.25;
    float altitude =  -1*( ((realTemperature + 273.15) * (pow((double)realPressure / (double)seaLevelPressure, 0.19022256f) - 1.0f  ) ) / 0.0065  );


  Serial.print("\t");
  Serial.print(altitude, 2);
  Serial.println();



  float N3=0;
 
   
   
        
    for(int cc=1;cc<=maxsamples;cc++){                                   // averager
        alt[(cc-1)]=alt[cc];
      
    };
    alt[maxsamples]=altitude;
  
  
    for(int cc=0;cc<maxsamples;cc++){
   
        N3+=(alt[cc]);
     
    };

     altAveraged = N3/maxsamples;

   


  Serial.print(realTemperature);
  Serial.print(":");
  Serial.print(realTemperature);
  Serial.print(":");
  Serial.print(realPressure);
  Serial.print(":");
  Serial.print(realPressure);
  Serial.print(":");
  Serial.print(altAveraged);
  Serial.print(":");
  Serial.print(altAveraged);
  Serial.println();

//  delay(1000);
}



// -- END OF FILE --
