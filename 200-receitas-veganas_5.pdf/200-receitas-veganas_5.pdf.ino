/*
  Analog Input

  Demonstrates analog input by reading an analog sensor on analog pin 0 and
  turning on and off a light emitting diode(LED) connected to digital pin 13.
  The amount of time the LED will be on and off depends on the value obtained
  by analogRead().

  The circuit:
  - potentiometer
    center pin of the potentiometer to the analog input 0
    one side pin (either one) to ground
    the other side pin to +5V
  - LED
    anode (long leg) attached to digital output 13 through 220 ohm resistor
    cathode (short leg) attached to ground

  - Note: because most Arduinos have a built-in LED attached to pin 13 on the
    board, the LED is optional.

  created by David Cuartielles
  modified 30 Aug 2011
  By Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogInput
*/

int sensorPin = A0;    // select the input pin for the potentiometer
int ledPin = 13;      // select the pin for the LED
int sensorValue = 0;  // variable to store the value coming from the sensor

int buzzer_pin = D0;  

void setup() {

    Serial.begin(115200);
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
    pinMode(buzzer_pin, OUTPUT);
    play_welcome_beep();
    
}

void loop() {
//  // read the value from the sensor:
//  sensorValue = analogRead(sensorPin);
//  // turn the ledPin on
//  digitalWrite(ledPin, HIGH);
//  // stop the program for <sensorValue> milliseconds:
//
//
//  Serial.println("Tone");
//   Serial.print(sensorValue);
// digitalWrite(buzzer_pin, HIGH);
//   delay(sensorValue*3);
//    digitalWrite(buzzer_pin, LOW);
//  // NewTone(buzzer_pin,0.1);
//   
//  // turn the ledPin off:
//  digitalWrite(ledPin, LOW);
//  // stop the program for for <sensorValue> milliseconds:
//  delay(sensorValue);
}

void play_welcome_beep()                 // Saudações de áudio
{

   delay(500);   
    for (int a=1;a<=10;a++)
    {
      digitalWrite(buzzer_pin, HIGH);    
        delay(50);
        digitalWrite(buzzer_pin, LOW); 
        delay(100);   
    }

 delay(500);   
    for (int a=1;a<=10;a++)
    {
      digitalWrite(buzzer_pin, HIGH);    
        delay(100);
        digitalWrite(buzzer_pin, LOW); 
        delay(50);   
    }

      delay(500);   
      
    for (int a=1;a<=10;a++)
    {
      digitalWrite(buzzer_pin, HIGH);    
        delay(100);
        digitalWrite(buzzer_pin, LOW); 
        delay(30);   
    }


    /////

      delay(500);   
      
    for (int a=1;a<=10;a++)
    {
      digitalWrite(buzzer_pin, HIGH);    
        delay(100);
        digitalWrite(buzzer_pin, LOW); 
        delay(10);   
    }

      delay(500);   
      
    for (int a=1;a<=10;a++)
    {
      digitalWrite(buzzer_pin, HIGH);    
        delay(500);
        digitalWrite(buzzer_pin, LOW); 
        delay(10);   
    }

     delay(500);   
      
    for (int a=1;a<=10;a++)
    {
      digitalWrite(buzzer_pin, HIGH);    
        delay(500);
        digitalWrite(buzzer_pin, LOW); 
        delay(50);   
    }
}
