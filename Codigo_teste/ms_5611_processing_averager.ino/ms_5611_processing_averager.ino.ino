/*
  MS5611 Barometric Pressure & Temperature Sensor. Output for MS5611_processing.pde
  Read more: http://www.jarzebski.pl/arduino/czujniki-i-sensory/czujnik-cisnienia-i-temperatury-ms5611.html
  GIT: https://github.com/jarzebski/Arduino-MS5611
  Web: http://www.jarzebski.pl
  (c) 2014 by Korneliusz Jarzebski
 */

#include <Wire.h>
#include <MS5611.h>

#include <SoftwareSerial.h>

SoftwareSerial btSerial(D3, D4); // Rx,Tx

#include "SSD1306Wire.h"
SSD1306Wire display(0x3c, SDA, SCL);

MS5611 ms5611;


unsigned char maxsamples=15;
float alt[51];

long leseZeitBT = 100;      //delay do bluetooth
long realPressure,  realPressure2;
long pressureFinal;

float altAveraged;//altura media

void setup() 
{
  Serial.begin(115200);

  btSerial.begin(9600); // bluetooth module baudrate

  // Initialize MS5611 sensor
  // Ultra high resolution: MS5611_ULTRA_HIGH_RES
  // (default) High resolution: MS5611_HIGH_RES
  // Standard: MS5611_STANDARD
  // Low power: MS5611_LOW_POWER
  // Ultra low power: MS5611_ULTRA_LOW_POWER
  while(!ms5611.begin(MS5611_ULTRA_HIGH_RES))
  {
    delay(500);
  }


 display.init();
 display.clear();
 display.flipScreenVertically();
}

void loop()
{
  // Read true temperature & Pressure (without compensation)
  double realTemperature = ms5611.readTemperature();
   realPressure = ms5611.readPressure();
  double realAltitude = ms5611.getAltitude(realPressure);

  // Read true temperature & Pressure (with compensation)
  double realTemperature2 = ms5611.readTemperature(true);
   realPressure2 = ms5611.readPressure(true);
 

  double realAltitude2 = ms5611.getAltitude(realPressure2);


  //averager antes de enviar na serial



 
 
    float N3=0;
 
   
   
        
    for(int cc=1;cc<=maxsamples;cc++){                                   // averager
        alt[(cc-1)]=alt[cc];
      
    };
    alt[maxsamples]=realAltitude2;
  
  
    for(int cc=0;cc<maxsamples;cc++){
   
        N3+=(alt[cc]);
     
    };

     altAveraged = N3/maxsamples;
   

    
  


  

  // Output
  Serial.print(realTemperature);
  Serial.print(":");
  Serial.print(realTemperature2);
  Serial.print(":");
  Serial.print(realPressure);
  Serial.print(":");
  Serial.print(realPressure2);
  Serial.print(":");
  Serial.print(realAltitude);
  Serial.print(":");
  Serial.print(altAveraged);
  Serial.println();



  bluetooth();


//-----------mostrar telas-----------
 
    draw();
 
  
}


void bluetooth(){
  
  // Start "Blue Fly Vario" sentence =============================================================================
  // =============================================================================================================
  /* Ausgabe im BlueFlyVario Format.     The standard BlueFlyVario outp ut mode. This sends raw
    pressure measurements in the form "PRS XXXXX\n": XXXXX is the raw (unfiltered) pressure
    measurement in hexadecimal pascals. */
  /*/ On-Off | Hier zwischen // ein * setzen dann ist es deaktiviert.
  Temp = bpm.readTemperature();
  //Druck = bpm.readPressure();
  Druck = 0.250* bpm.readPressure(true) +  0.750* Druck;
  Serial.print("PRS ");               //Ausgabe an der BT fuer MiniPro.
  Serial.println( Druck, HEX);        //BT-Serial Schnittstelle ungefiltert.  Fuer MiniPro.
  //Serial1.print("PRS ");               //Ausgabe an der BT fuer Leonardo.
  //Serial1.println( Druck, HEX);        //BT-Serial Schnittstelle ungefiltert.  Fuer Leonardo.
  delay(leseZeitBT - 73);
  // Wenn XCSoar verwendet wird die Zeile drunter mit "//..." auskommentieren.
  //delay(leseZeitBT - 22); //Wenn XCTrack benutzt wird Zeile aktiv lassen.
  // Ende "BlueFlyVario" sentence =========================================================================== */



  pressureFinal = 0.250* realPressure +  0.750* realPressure2;
  btSerial.print("PRS ");               //Ausgabe an der BT fuer MiniPro.
  btSerial.println( pressureFinal, HEX); 

    delay(leseZeitBT - 22); 
  
}


void draw(void) {
display.clear();
 
display.setFont(ArialMT_Plain_16);
display.drawString(0, 0, "Altura");
 
display.setFont(ArialMT_Plain_16);
display.drawString(0, 20,  String(altAveraged,2) );

 
display.setFont(ArialMT_Plain_10);
display.drawString(0, 50, "Thainã Monteiro");

display.display();
  

}
